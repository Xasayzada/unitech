FROM maven:3.8.2-jdk-11
COPY . ./unitech
WORKDIR /unitech
RUN mvn clean install -DskipTests
CMD mvn spring-boot:run