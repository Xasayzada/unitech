package com.demo.unitech.unit;

import com.demo.unitech.exception.UserExistException;
import com.demo.unitech.model.User;
import com.demo.unitech.model.UserRegisterRequest;
import com.demo.unitech.repo.Mapper;
import com.demo.unitech.service.impl.RegisterServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
@ExtendWith(MockitoExtension.class)
public class RegisterServiceImplTest {

    @Mock
    Mapper mapper;

    @InjectMocks
    RegisterServiceImpl registerService;


    @Test
    void registerUserTest() {
        String pin = "test";
        UserRegisterRequest request = UserRegisterRequest.builder().pin(pin).password("test").build();
        Mockito.when(mapper.findUserByPin(pin)).thenReturn(Optional.of(new User()));
        assertThrows(UserExistException.class, () -> registerService.registerUser(request));
        Mockito.verify(mapper, Mockito.times(1)).findUserByPin(pin);
    }
}
