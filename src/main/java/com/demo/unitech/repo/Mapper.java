package com.demo.unitech.repo;


import com.demo.unitech.model.AccountDto;
import com.demo.unitech.model.User;
import com.demo.unitech.model.UserRegisterRequest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@org.apache.ibatis.annotations.Mapper
public interface Mapper {

    Optional<User> findUserByPin(String pin);

    void insertUser(UserRegisterRequest request);

    List<AccountDto> getAccountsByPin(String pin );
    Optional<AccountDto> getAccountsByAccountNumber(  String accountNumber);

    void updateAccount(String accountNumber, BigDecimal amount, Boolean increase);
}
