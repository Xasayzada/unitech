package com.demo.unitech.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserExistException  extends RuntimeException{
    private String pin;
}
