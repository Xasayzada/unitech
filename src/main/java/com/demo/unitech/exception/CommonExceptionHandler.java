package com.demo.unitech.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class CommonExceptionHandler{


    @ExceptionHandler( UserExistException.class)
    public ResponseEntity<ErrorResponse> handleException(UserExistException e) {
        log.error("UserExistException occurred ", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse(String.format("User with pin  '%s' already exist", e.getPin())));
    }

    @ExceptionHandler( CommonException.class)
    public ResponseEntity<ErrorResponse> handleException(CommonException e){
        log.error("CommonException occurred ", e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ErrorResponse("Common exception occured."));
    }

    @ExceptionHandler( NotFoundException.class)
    public ResponseEntity<ErrorResponse> handleException(NotFoundException e){
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse("Data not found."));
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorResponse> handleException(BadCredentialsException e){
        log.error("BadCredentials exception occurred ", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse("Pin or password is incorrect"));
    }

    @ExceptionHandler(SameAccountTransferException.class)
    public ResponseEntity<ErrorResponse> handleException(SameAccountTransferException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse("you cannot transfer to the same account"));
    }

    @ExceptionHandler(NotExistingAccount.class)
    public ResponseEntity<ErrorResponse> handleException(NotExistingAccount e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse("Account doesn't exist"));
    }

    @ExceptionHandler(NotEnoughMoneyException.class)
    public ResponseEntity<ErrorResponse> handleException(NotEnoughMoneyException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse("Not enough money in balance"));
    }

    @ExceptionHandler(DeactiveAccountException.class)
    public ResponseEntity<ErrorResponse> handleException(DeactiveAccountException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse("Account is deactive"));
    }

    @ExceptionHandler(AccountNotBelongToCurrentUserException.class)
    public ResponseEntity<ErrorResponse> handleException(AccountNotBelongToCurrentUserException e){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse("Account doesn't belong to current user"));
    }
}
