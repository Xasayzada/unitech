package com.demo.unitech.exception;

import lombok.Getter;

@Getter
public class CommonException extends RuntimeException{

    private Throwable cause;
    public CommonException(Throwable cause) {
        this.cause = cause;
    }
}
