package com.demo.unitech.filter;

import com.demo.unitech.service.impl.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class CustomAuthFilter extends OncePerRequestFilter {

    private final JwtService jwtService;


    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if (!request.getRequestURI().startsWith("/api/public")){
            String xAuthorizationHeader = request.getHeader("X-Authorization");
            String jwToken = this.getJwt(xAuthorizationHeader);
            if (xAuthorizationHeader != null && this.jwtService.validateToken(jwToken)) {
                filterChain.doFilter(request,response);
            } else {
                response.setStatus(401);
                return;
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }


    private String getJwt(String header) {
        if (header == null) {
            return null;
        } else {
            if (header.length() > 7) {
                header = header.substring(7);
            }

            return header;
        }
    }

}
