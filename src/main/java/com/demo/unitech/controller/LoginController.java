package com.demo.unitech.controller;

import com.demo.unitech.model.LoginRequest;
import com.demo.unitech.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;


    @PostMapping("/public/login")
    public ResponseEntity login(@RequestBody LoginRequest request){
        HttpHeaders httpHeaders =  new HttpHeaders();
        loginService.login(request, httpHeaders);
        return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    }
}
