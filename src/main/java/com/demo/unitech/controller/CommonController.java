package com.demo.unitech.controller;

import com.demo.unitech.model.CurrencyRate;
import com.demo.unitech.service.CommonService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CommonController {

    CommonService commonService;


    @GetMapping("/private/currency")
    public CurrencyRate getCurrencyRate(@RequestParam String id){
        return commonService.getCurrencyRate(id);
    }
}
