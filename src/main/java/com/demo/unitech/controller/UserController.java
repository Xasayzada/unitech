package com.demo.unitech.controller;

import com.demo.unitech.model.Accounts;
import com.demo.unitech.model.TransferRequest;
import com.demo.unitech.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping("/private/user/account/list")
    public Accounts getAccounts (){
        return userService.getAccounts();
    }


    @PostMapping("/private/account/transfer")
    public ResponseEntity transferRequest(@RequestBody TransferRequest request){
        userService.transferRequest(request);
        return new ResponseEntity(HttpStatus.OK);
    }

}
