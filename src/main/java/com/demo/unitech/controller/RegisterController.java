package com.demo.unitech.controller;

import com.demo.unitech.model.UserRegisterRequest;
import com.demo.unitech.service.RegisterService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE , makeFinal = true)
public class RegisterController {

    RegisterService registerService;

    @PostMapping("/public/register")
    public ResponseEntity<Object> registerUser(@RequestBody UserRegisterRequest request){
        registerService.registerUser(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
