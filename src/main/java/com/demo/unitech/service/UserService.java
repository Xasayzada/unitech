package com.demo.unitech.service;

import com.demo.unitech.model.Accounts;
import com.demo.unitech.model.TransferRequest;

public interface UserService {
    Accounts getAccounts();

    void transferRequest(TransferRequest request);
}
