package com.demo.unitech.service.impl;

import com.demo.unitech.exception.UserExistException;
import com.demo.unitech.model.UserRegisterRequest;
import com.demo.unitech.repo.Mapper;
import com.demo.unitech.service.RegisterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegisterServiceImpl implements RegisterService {

    private final BCryptPasswordEncoder passwordEncoder;
    private final Mapper userMapper;

    @Override
    public void registerUser(UserRegisterRequest request) {
        if (validateIfUserExist(request.getPin())) {
            throw new UserExistException(request.getPin());
        }
        request.setPassword( passwordEncoder.encode(request.getPassword()));
        userMapper.insertUser(request);
        log.info("User with pin - "  + request.getPin() + " registered successfully" );
    }



    public boolean validateIfUserExist(String pin) throws UserExistException {
        return userMapper.findUserByPin(pin).isPresent();
    }
}
