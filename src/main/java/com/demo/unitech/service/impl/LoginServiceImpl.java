package com.demo.unitech.service.impl;

import com.demo.unitech.model.LoginRequest;
import com.demo.unitech.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    @Override
    public void login(LoginRequest request, HttpHeaders httpHeaders) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getPin(),request.getPassword()));
        String token = jwtService.generateToken(authentication);
        httpHeaders.set("X-Authorization" , token);
    }
}
