package com.demo.unitech.service.impl;

import com.demo.unitech.exception.NotFoundException;
import com.demo.unitech.model.CurrencyRate;
import com.demo.unitech.proxy.ProxyService;
import com.demo.unitech.service.CommonService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommonServiceImpl implements CommonService {

    private final CacheService cacheService;

    @Override
    public CurrencyRate getCurrencyRate(String id) {
        return Optional.ofNullable(cacheService.getCurrencyRate().get(id)).orElseThrow(() -> new NotFoundException());
    }



}
