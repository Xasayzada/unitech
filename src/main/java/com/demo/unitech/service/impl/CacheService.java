package com.demo.unitech.service.impl;

import com.demo.unitech.model.CurrencyRate;
import com.demo.unitech.model.CurrencyRates;
import com.demo.unitech.proxy.ProxyService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CacheService {
    private final ProxyService proxyService;

    @Cacheable("currencies")
    public Map<String, CurrencyRate> getCurrencyRate() {
         CurrencyRates currencyRates = proxyService.getCurrencyRates();
         return currencyRates.getRateList().stream()
                 .collect(Collectors.toMap(CurrencyRate::getCurrencyPair , Function.identity()));
    }
}
