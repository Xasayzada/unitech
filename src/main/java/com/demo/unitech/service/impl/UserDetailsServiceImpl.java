package com.demo.unitech.service.impl;

import com.demo.unitech.exception.NotFoundException;
import com.demo.unitech.exception.UserExistException;
import com.demo.unitech.model.User;
import com.demo.unitech.repo.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Mapper userMapper;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userMapper.findUserByPin(username).orElseThrow(() -> new BadCredentialsException(""));
        return new org.springframework.security.core.userdetails.User(user.getPin(), user.getPassword(),true,
                user.isActive(),true,true, Collections.EMPTY_LIST);
    }
}
