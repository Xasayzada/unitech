package com.demo.unitech.service.impl;

import com.demo.unitech.model.AccountDto;
import com.demo.unitech.model.Accounts;
import com.demo.unitech.model.TransferRequest;
import com.demo.unitech.model.TransferValidationBuilder;
import com.demo.unitech.repo.Mapper;
import com.demo.unitech.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final Mapper mapper;
    private final JwtService jwtService;

    @Override
    public Accounts getAccounts() {
        String pin = jwtService.getPinFromToken();
        return Accounts.builder().accountList(mapper.getAccountsByPin(pin)).build();
    }


    @Override
    @Transactional
    public void transferRequest(TransferRequest request) {
        String pin = jwtService.getPinFromToken();

         AccountDto accountDto = null;
         AccountDto accountDtoFrom = null;

        TransferValidationBuilder.builder()
                .isDifferentAccount(!request.getTransferFrom().equals(request.getTransferTo()))
                .isExistingAccount((accountDto = mapper.getAccountsByAccountNumber(request.getTransferTo()).get())!=null)
                .isActiveAccount(accountDto.getIsActive())
                .isAccountBelongToCurrentUser((accountDtoFrom = mapper.getAccountsByAccountNumber(request.getTransferFrom()).get()).getUserPin().equals(pin))
                .isEnoughMoney( accountDtoFrom.getBalance().compareTo(request.getAmount()) != -1)
                .build();


        mapper.updateAccount(request.getTransferFrom(), request.getAmount(), false);
        mapper.updateAccount(request.getTransferTo(), request.getAmount(), true);

    }
}
