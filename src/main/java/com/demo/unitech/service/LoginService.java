package com.demo.unitech.service;

import com.demo.unitech.model.LoginRequest;
import org.springframework.http.HttpHeaders;

public interface LoginService {
    void login(LoginRequest request, HttpHeaders httpHeaders);

}
