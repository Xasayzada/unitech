package com.demo.unitech.service;

import com.demo.unitech.model.CurrencyRate;

public interface CommonService {
    CurrencyRate getCurrencyRate(String id);
}
