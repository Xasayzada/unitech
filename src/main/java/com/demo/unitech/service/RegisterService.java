package com.demo.unitech.service;

import com.demo.unitech.model.UserRegisterRequest;

public interface RegisterService {

    void registerUser(UserRegisterRequest request);
}
