package com.demo.unitech.proxy;

import com.demo.unitech.model.CurrencyRate;
import com.demo.unitech.model.CurrencyRates;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class ProxyService {

    public CurrencyRates getCurrencyRates() {
        return CurrencyRates.builder()
                .rateList(getCurrencyRatesMock())
                .build();
    }


    private List<CurrencyRate> getCurrencyRatesMock() {
//        CurrencyRate  USDtoAZN = new  ("USD/AZN" , BigDecimal.valueOf(1.7));
//        CurrencyRate  AZNtoUSD = new CurrencyRate("AZN/USD" , BigDecimal.valueOf(0.59));
//        CurrencyRate  AZNtoEUR = new CurrencyRate("AZN/EUR" , BigDecimal.valueOf(0.58));
//        CurrencyRate  EURtoAZN = new CurrencyRate("EUR/AZN" , BigDecimal.valueOf(1.73));


        Random random = new Random();
        CurrencyRate USDtoAZN = new CurrencyRate("USD/AZN", BigDecimal.valueOf(random.nextDouble()));
        CurrencyRate AZNtoUSD = new CurrencyRate("AZN/USD", BigDecimal.valueOf(random.nextDouble()));
        CurrencyRate AZNtoEUR = new CurrencyRate("AZN/EUR", BigDecimal.valueOf(random.nextDouble()));
        CurrencyRate EURtoAZN = new CurrencyRate("EUR/AZN", BigDecimal.valueOf(random.nextDouble()));

        return Arrays.asList(USDtoAZN, AZNtoEUR, EURtoAZN, AZNtoUSD);
    }
}
