package com.demo.unitech.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class TransferRequest {

    private String transferFrom;
    private String transferTo;
    private BigDecimal amount;
}
