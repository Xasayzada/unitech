package com.demo.unitech.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CurrencyRates {
    private List<CurrencyRate> rateList;
}
