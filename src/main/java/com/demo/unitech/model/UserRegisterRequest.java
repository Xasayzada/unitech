package com.demo.unitech.model;

import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class UserRegisterRequest {

    @NonNull
    private String pin;
    private String name;
    private String surname;
    @ToString.Exclude
    @NonNull
    private String password;
}
