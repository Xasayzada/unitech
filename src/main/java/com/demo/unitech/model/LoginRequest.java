package com.demo.unitech.model;

import lombok.Getter;

@Getter
public class LoginRequest {
    private String pin;
    private String password;
}
