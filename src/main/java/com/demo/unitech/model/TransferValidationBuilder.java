package com.demo.unitech.model;


import com.demo.unitech.exception.*;

public class TransferValidationBuilder {
    private boolean isAccountBelongToCurrentUser;
    private boolean isEnoughMoney;
    private boolean isDifferentAccount;
    private boolean isActiveAccount;
    private boolean isExistingAccount;

    public static TransferValidationBuilder.TransferValidationBuilderBuilder builder() {
        return new TransferValidationBuilder.TransferValidationBuilderBuilder();
    }

    public TransferValidationBuilder(final boolean isAccountBelongToCurrentUser, final boolean isEnoughMoney, final boolean isDifferentAccount, final boolean isActiveAccount, final boolean isExistingAccount) {
        this.isAccountBelongToCurrentUser = isAccountBelongToCurrentUser;
        this.isEnoughMoney = isEnoughMoney;
        this.isDifferentAccount = isDifferentAccount;
        this.isActiveAccount = isActiveAccount;
        this.isExistingAccount = isExistingAccount;
    }

    public TransferValidationBuilder() {
    }

    public boolean isAccountBelongToCurrentUser() {
        return this.isAccountBelongToCurrentUser;
    }

    public boolean isEnoughMoney() {
        return this.isEnoughMoney;
    }

    public boolean isDifferentAccount() {
        return this.isDifferentAccount;
    }

    public boolean isActiveAccount() {
        return this.isActiveAccount;
    }

    public boolean isExistingAccount() {
        return this.isExistingAccount;
    }

    public void setAccountBelongToCurrentUser(final boolean isAccountBelongToCurrentUser) {
        this.isAccountBelongToCurrentUser = isAccountBelongToCurrentUser;
    }

    public void setEnoughMoney(final boolean isEnoughMoney) {
        this.isEnoughMoney = isEnoughMoney;
    }

    public void setDifferentAccount(final boolean isDifferentAccount) {
        this.isDifferentAccount = isDifferentAccount;
    }

    public void setActiveAccount(final boolean isActiveAccount) {
        this.isActiveAccount = isActiveAccount;
    }

    public void setExistingAccount(final boolean isExistingAccount) {
        this.isExistingAccount = isExistingAccount;
    }



    public static class TransferValidationBuilderBuilder {
        private boolean isAccountBelongToCurrentUser;
        private boolean isEnoughMoney;
        private boolean isDifferentAccount;
        private boolean isActiveAccount;
        private boolean isExistingAccount;

        TransferValidationBuilderBuilder() {
        }

        public TransferValidationBuilder.TransferValidationBuilderBuilder isAccountBelongToCurrentUser(final boolean isAccountBelongToCurrentUser) {
            if (!isAccountBelongToCurrentUser){
                throw new AccountNotBelongToCurrentUserException();
            }
            return this;
        }

        public TransferValidationBuilder.TransferValidationBuilderBuilder isEnoughMoney(final boolean isEnoughMoney) {
            if (!isEnoughMoney){
                throw new NotEnoughMoneyException();
            }
            return this;
        }

        public TransferValidationBuilder.TransferValidationBuilderBuilder isDifferentAccount(final boolean isDifferentAccount) {
            if (! isDifferentAccount){
                throw new SameAccountTransferException();
            }
            return this;
        }

        public TransferValidationBuilder.TransferValidationBuilderBuilder isActiveAccount(final boolean isActiveAccount) {
            if (! isActiveAccount){
                throw new DeactiveAccountException();
            }
            return this;
        }

        public TransferValidationBuilder.TransferValidationBuilderBuilder isExistingAccount(final boolean isExistingAccount) {
            if (! isExistingAccount){
                throw new NotExistingAccount();
            }
            return this;
        }

        public TransferValidationBuilder build() {
            return new TransferValidationBuilder(this.isAccountBelongToCurrentUser, this.isEnoughMoney, this.isDifferentAccount, this.isActiveAccount, this.isExistingAccount);
        }

        public String toString() {
            return "TransferValidationBuilder.TransferValidationBuilderBuilder(isAccountBelongToCurrentUser=" + this.isAccountBelongToCurrentUser + ", isEnoughMoney=" + this.isEnoughMoney + ", isDifferentAccount=" + this.isDifferentAccount + ", isActiveAccount=" + this.isActiveAccount + ", isExistingAccount=" + this.isExistingAccount + ")";
        }
    }
}

