package com.demo.unitech.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class AccountDto {
    private String accountNumber;
    private String currency;
    private BigDecimal balance;
    private Boolean isActive;
    private String userPin;
}
